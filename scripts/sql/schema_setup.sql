CREATE TABLE IF NOT EXISTS power_prices (
    OPERATION_DATE DATE,
    HOUR_ENDING INT,
    INTERVAL_ENDING INT,
    TOUTYPE VARCHAR(50),
    MARKET VARCHAR(50), 
    ZONE VARCHAR(50),
    PRICE MONEY
);
CREATE TABLE IF NOT EXISTS power_usage (
    local_interval_time TIMESTAMP,
    lp_value REAL,
    lat FLOAT,
    long FLOAT,
    u_id INT
);

CREATE TABLE IF NOT EXISTS sa_trip (
	DetectionType INT,
	DetName VARCHAR(50),
	LocationName VARCHAR(50),
	LocationName2 VARCHAR(50),
	AssetNum INT,
	Latitude FLOAT,
	Longitude FLOAT,
	Direction VARCHAR(50),
	Lanes VARCHAR(50),
	"Timestamp" TIMESTAMP,
	Volume INT,
	Occupancy FLOAT,
	Speed FLOAT,
	Health INT,
	"Upload Success" FLOAT,
	ExportDate TIMESTAMP
);
CREATE TABLE IF NOT EXISTS bus_adherence (
    ServiceDate DATE,
    Routes INT,
    Block VARCHAR(15),
    RouteDirectionName VARCHAR(15),
    StopNumber VARCHAR(25),
    Location VARCHAR(255),
    Latitude LONG,
    Longitude LONG,
    ScheduledTime(s) INT,
    ScheduledTime(HHMMSS) TIME,
    ArrivalTime(s) INT,
    ArrivalTime(HHMMSS) TIME,
    DepartureTime(s) INT,
    DepartureTime(HHMMSS) TIME,
    Odometer REAL,
    VehicleNumber INT
);
CREATE TABLE IF NOT EXISTS bus_fare_activity (
    ServiceDate DATE,
    Route INT,
    RouteName VARCHAR(50),
    CurrentRevenue MONEY,
    Ridership INT,
    TokenCount INT,
    TicketCount INT,
    PassCount INT,
    BillCount INT,
    UnclassifiedRevenue MONEY,
    DumpCount INT
);

CREATE TABLE IF NOT EXISTS bus_OnTimePerformance (
    ServiceDateTime DATE,
    Route INT,
    RouteName VARCHAR(50),
    EarlyDeparture INT,
    OnTime INT,
    LateArrival INT,
    Missing INT,
    TimePointCount INT,
    OTP REAL,
    AverageDwellTime REAL
);

CREATE TABLE IF NOT EXISTS bus_loggedmessages (
    Date DATE,
    Local_Timestamp TIMESTAMP,
    Latitude LONG,
    Longitude LONG,
    MESSAGE_TYPE_TEXT VARCHAR(255),
    Adherence REAL,
    Odometer REAL,
    VehicleNumber INT
);

CREATE TABLE IF NOT EXISTS air_quality (
	"Airmonitor Name" VARCHAR(50),
	"Airmonitor Active" VARCHAR(50) ,
	"Airmonitor Address Zip" INT,
	"Airmonitor Address State" VARCHAR(25),
	"Airmonitor Address City" VARCHAR(50),
	"Airmonitor Address Street" VARCHAR(100),
	"AirmonitorReading TimeStamp" TIMESTAMP,
	"AirmonitorReading Temperature" INT,
	"AirmonitorReading Humidity" INT,
	"AirmonitorReading NO2" INT,
	"AirmonitorReading NO2 AQI" INT,
	"AirmonitorReading SO2" INT,
	"AirmonitorReading SO2 AQI" INT,
	"AirmonitorReading O3" INT,
	"AirmonitorReading O3 AQI" INT,
	"AirmonitorReading CO" INT,
	"AirmonitorReading CO AQI" INT,
	"AirmonitorReading VOC" INT,
	"AirmonitorReading VOC AQI" INT,
	"AirmonitorReading PM2Point5" INT,
	"AirmonitorReading PM2Point5 AQI" INT,
	"AirmonitorReading PM10" INT,
	"AirmonitorReading PM10 AQI" INT,
	"AirmonitorReading AirmonitorReading FinalAQI" INT
);

CREATE TABLE IF NOT EXISTS three_one_one (
    CASEID INT,
    CASEREF VARCHAR(255),
    OPENEDDATETIME TIMESTAMP,
    CLOSEDDATETIME TIMESTAMP,
    SLA_Date TIMESTAMP,
    "Subject To SLA" BOOLEAN,
    "Late (Yes/No)" BOOLEAN,
    LATEDAYS FLOAT,
    "Closed (Yes/No)" BOOLEAN,
    Dept VARCHAR(100),
    SUBJECTNAME VARCHAR(100),
    REASONNAME VARCHAR(100),
    TYPENAME VARCHAR(200),
    SLADays FLOAT,
    CaseStatus VARCHAR(100),
    SOURCEID VARCHAR(100),
    SOURCEUSERID VARCHAR(100),
    OBJECTDESC VARCHAR(255),
    "Council District" INT,
    ALLOCATEDTODEPTID VARCHAR(50),
    " interactiondate" TIMESTAMP,
    CLIENTID VARCHAR(100),
    XCOORD INT,
    YCOORD INT,
    "Select Dept?" BOOLEAN,
    "Dept (to)" VARCHAR(100),
    "Report Starting Date" TIMESTAMP,
    "Report Ending Date" TIMESTAMP
);


/*SARA*/

CREATE TABLE IF NOT EXISTS sara_rainfall_detail_data (
   "location_name" VARCHAR(100),
   "latitude" FLOAT,
   "longitude" FLOAT,
   "date_time" TIMESTAMP,
   "five_minute_rainfall" FLOAT
 );


CREATE TABLE IF NOT EXISTS sara_rainfall_summary_data (
    "location_name" VARCHAR(100),
    "latitude" FLOAT,
    "longitude" FLOAT,
    "date" TIMESTAMP,
    "daily_rainfall_total_inches" FLOAT
 );

CREATE TABLE IF NOT EXISTS sara_rainfall_stage_daily_average (
    "location_name" VARCHAR(100),
    "latitude" FLOAT,
    "longitude" FLOAT,
    "date" TIMESTAMP,
    "daily_average_stage" FLOAT,
    "tranducer_elevation" FLOAT
 );

/*EAA*/

CREATE TABLE IF NOT EXISTS eaa_spring_water_quality(
    "Date-Time" TIMESTAMP,
    "O2 (Dis) mg/l" FLOAT,
    "Nephelom NTU" FLOAT,
    "pH" FLOAT,
    "Sp Cond muS/cm" FLOAT,
    "Temperature iC" FLOAT,
    "Name"  VARCHAR(200),
    Latitude FLOAT,
    Longitude FLOAT
 );
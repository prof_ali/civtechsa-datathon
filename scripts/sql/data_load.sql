\COPY power_prices (
    OPERATION_DATE,
    HOUR_ENDING,
    INTERVAL_ENDING,
    TOUTYPE,
    MARKET, 
    ZONE,
    PRICE
)
FROM './data/Price_2017_CPS-2.csv'
WITH DELIMITER ',' CSV HEADER;

\COPY sa_trip(
	DetectionType,
	DetName,
	LocationName,
	LocationName2,
	AssetNum,
	Latitude,
	Longitude,
	Direction,
	Lanes,
	"Timestamp",
	Volume,
	Occupancy,
	Speed,
	Health,
	"Upload Success",
	ExportDate
)
FROM './data/SATRIP_detectionsummary.csv'
WITH DELIMITER ',' CSV HEADER; 

\COPY air_quality (
	"Airmonitor Name", 
	"Airmonitor Active",
	"Airmonitor Address Zip",
	"Airmonitor Address State",
	"Airmonitor Address City",
	"Airmonitor Address Street",
	"AirmonitorReading TimeStamp",
	"AirmonitorReading Temperature",
	"AirmonitorReading Humidity",
	"AirmonitorReading NO2",
	"AirmonitorReading NO2 AQI",
	"AirmonitorReading SO2",
	"AirmonitorReading SO2 AQI",
	"AirmonitorReading O3",
	"AirmonitorReading O3 AQI",
	"AirmonitorReading CO",
	"AirmonitorReading CO AQI",
	"AirmonitorReading VOC",
	"AirmonitorReading VOC AQI",
	"AirmonitorReading PM2Point5",
	"AirmonitorReading PM2Point5 AQI",
	"AirmonitorReading PM10", 
	"AirmonitorReading PM10 AQI",
	"AirmonitorReading AirmonitorReading FinalAQI"
)
FROM './data/COSA Digital Kiosks Air Monitor Readings (Feb 4, 2019 - May 4, 2019).csv'
WITH DELIMITER ',' CSV HEADER;

\COPY three_one_one(
    CASEID,
    CASEREF,
    OPENEDDATETIME,
    CLOSEDDATETIME,
    SLA_Date,
    "Subject To SLA",
    "Late (Yes/No)",
    LATEDAYS,
    "Closed (Yes/No)",
    Dept,
    SUBJECTNAME,
    REASONNAME,
    TYPENAME,
    SLADays,
    CaseStatus,
    SOURCEID,
    SOURCEUSERID,
    OBJECTDESC,
    "Council District",
    ALLOCATEDTODEPTID,
    " interactiondate",
    CLIENTID,
    XCOORD,
    YCOORD,
    "Select Dept?",
    "Dept (to)",
    "Report Starting Date",
    "Report Ending Date"
)
FROM './data/311 Data_LaganExtract_20180101_20181231.csv'
WITH DELIMITER ',' CSV HEADER; 

/* SARA(San Antonio River Authority) DATASETS */

\COPY sara_rainfall_detail_data(
   "location_name",
   "latitude",
   "longitude",
   "date_time",
   "five_minute_rainfall"
)
FROM './data/RainfallDetailData.csv'
WITH DELIMITER ',' CSV HEADER;

\COPY sara_rainfall_summary_data(
    "location_name",
    "latitude",
    "longitude",
    "date",
    "daily_rainfall_total_inches"
)
FROM './data/RainfallSummaryData.csv'
WITH DELIMITER ',' CSV HEADER; 

\COPY sara_rainfall_stage_daily_average(
    "location_name",
    "latitude",
    "longitude",
    "date",
    "daily_average_stage",
    "tranducer_elevation"
)
FROM './data/RiverStageDailyAverage.csv'
WITH DELIMITER ',' CSV HEADER;

/*Edwards Acquifier Dataset*/

\COPY eaa_spring_water_quality(
        "Date-Time",
        "O2 (Dis) mg/l",
        "Nephelom NTU",
        "pH",
        "Sp Cond muS/cm",
        "Temperature iC",
        "Name",
        Latitude,
        Longitude
)
FROM './data/EAA.csv'
WITH DELIMITER ',' CSV HEADER; 

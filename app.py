################################################################################
# Script     : app.py
# Authors    : David Velez, Harry Staley, Josh Davenport, Artem Skitenko,
#              Navin Matthews, Izzat Alsmadi
# Date       : 06/29/2018
# Description: Using Plotly's Dash, we are using their framework that is a
#              flask backend for data visualization and using D3's libraries
################################################################################

# Imports
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.graph_objs as go
import pandas as pd
import os

UPLOAD_DIRECTORY = "./data"

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)
    
    
app = dash.Dash(external_stylesheets=[dbc.themes.SLATE])

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
mapbox_access_token = "pk.eyJ1IjoiZHZlbGV6IiwiYSI6ImNqeGZieWpqbDBqa2kzenBlcWR4OXl3dDUifQ.hCS_L_yDfSajKk0lfyGWuQ"

colors = {
    "background": "#E7E7E7",
    "text": "#e6e6e6"
}

############################################################################
# Read CSVs Files
############################################################################

eaa_o2 = pd.read_csv("Data_Sources/EAA_O2_2018.csv", low_memory=False).dropna()
eaa_ph = pd.read_csv("Data_Sources/EAA_pH_2018.csv", low_memory=False).dropna()
eaa_neph = pd.read_csv("Data_Sources/EAA_Nephelom_2018.csv", low_memory=False).dropna()
eaa_temps = pd.read_csv("Data_Sources/EAA_TempCelsius_2018.csv", low_memory=False).dropna()
bus_locs = pd.read_csv("Data_Sources/VIA-Bus-Locations.csv", low_memory=False).dropna()
cps_ev = pd.read_csv("Data_Sources/EV_Data_3_yr_Session-Details-Summary-20180630.csv", low_memory=False)
cps_ev_small = pd.read_csv("Data_Sources/EV_Data_2018_Top3.csv", low_memory=False)
cps_ev_locs = pd.read_csv("Data_Sources/EV_Data_Locations.csv", low_memory=False)
via_riders = pd.read_csv("Data_Sources/VIA_BusFareBoxActivity_2018_Top3.csv", low_memory=False).dropna()
via_adh = pd.read_csv("Data_Sources/VIA_Adherence_Aug28_SixBuses.csv", low_memory=False)

############################################################################
# VIA Adherence
############################################################################
via_trace0 = go.Bar(
    x=[],
    y=[],
    name="",
    marker=dict(
        color="midnightblue"
    )
)

via_trace1 = go.Bar(
    x=[],
    y=[],
    name="",
    marker=dict(
        color="silver"
    )
)

via_trace2 = go.Bar(
    x=[],
    y=[],
    name="",
    marker=dict(
        color="orange"
    )
)

for i in via_adh.Route.unique():
    x = via_adh[via_adh["Route"] == i]["RouteName"]
    y = via_adh[via_adh["Route"] == i]["Ridership"]
    via_trace0['x'] += tuple(x)
    via_trace0['y'] += tuple(y)
    via_trace0['name'] = "Ridership"

for i in via_adh.Route.unique():
    x = via_adh[via_adh["Route"] == i]["RouteName"]
    y = via_adh[via_adh["Route"] == i]["PassCount"]
    via_trace1['x'] += tuple(x)
    via_trace1['y'] += tuple(y)
    via_trace1['name'] = "Pass Count"

for i in via_adh.Route.unique():
    x = via_adh[via_adh["Route"] == i]["RouteName"]
    y = via_adh[via_adh["Route"] == i]["BillCount"]
    via_trace2['x'] += tuple(x)
    via_trace2['y'] += tuple(y)
    via_trace2['name'] = "Bill Count"


############################################################################
# EV Data
############################################################################
ev_trace0 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="maroon"
    ),
    marker={
        "size": 15,
        "line": {
            "width": 0.5,
            "color": "white"
        }
    }
)

ev_trace1 = go.Scatter(
    x=[],
    y=[],
    mode="lines",
    name="",
    opacity=0.7,
    line=dict(
        color="dodgerblue"
    ),
    marker={
        "size": 15,
        "line": {
            "width": 0.5,
            "color": "white"
        }
    }
)

ev_trace2 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="green"
    ),
    marker={
        "size": 15,
        "line": {
            "width": 0.5,
            "color": "white"
        }
    }
)

ev_trace3 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="maroon",
        dash='dash'
    )
)

ev_trace4 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="dodgerblue",
        dash='dash'
    )
)

ev_trace5 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="green",
        dash='dash'
    )
)

for i in cps_ev.StationName.unique():
    if "PALLADIUM" in i or "RACKSPACE" in i:
        continue
    x = cps_ev_small[cps_ev_small["StationName"] == i]["Start Date"]
    y = cps_ev_small[cps_ev_small["StationName"] == i]["Energy (kWh)"]
    ev_trace0['x'] += tuple(x)
    ev_trace0['y'] += tuple(y)
    ev_trace0['name'] = "ST MARYS 1 (Energy)"

for i in cps_ev.StationName.unique():
    if "MARYS" in i or "RACKSPACE" in i:
        continue
    x = cps_ev_small[cps_ev_small["StationName"] == i]["Start Date"]
    y = cps_ev_small[cps_ev_small["StationName"] == i]["Energy (kWh)"]
    ev_trace1['x'] += tuple(x)
    ev_trace1['y'] += tuple(y)
    ev_trace1['name'] = "PALLADIUM STAT3 (Energy)"

for i in cps_ev.StationName.unique():
    if "PALLADIUM" in i or "MARYS" in i:
        continue
    x = cps_ev_small[cps_ev_small["StationName"] == i]["Start Date"]
    y = cps_ev_small[cps_ev_small["StationName"] == i]["Energy (kWh)"]
    ev_trace2['x'] += tuple(x)
    ev_trace2['y'] += tuple(y)
    ev_trace2['name'] = "RACKSPACE 1 (Energy)"

for i in cps_ev.StationName.unique():
    if "PALLADIUM" in i or "RACKSPACE" in i:
        continue
    x = cps_ev_small[cps_ev_small["StationName"] == i]["Start Date"]
    y = cps_ev_small[cps_ev_small["StationName"] == i]["GHG Savings (kg)"]
    ev_trace3['x'] += tuple(x)
    ev_trace3['y'] += tuple(y)
    ev_trace3['name'] = "ST MARYS 1 (GHG)"

for i in cps_ev.StationName.unique():
    if "MARYS" in i or "RACKSPACE" in i:
        continue
    x = cps_ev_small[cps_ev_small["StationName"] == i]["Start Date"]
    y = cps_ev_small[cps_ev_small["StationName"] == i]["GHG Savings (kg)"]
    ev_trace4['x'] += tuple(x)
    ev_trace4['y'] += tuple(y)
    ev_trace4['name'] = "PALLADIUM STAT3 (GHG)"

for i in cps_ev.StationName.unique():
    if "PALLADIUM" in i or "MARYS" in i:
        continue
    x = cps_ev_small[cps_ev_small["StationName"] == i]["Start Date"]
    y = cps_ev_small[cps_ev_small["StationName"] == i]["GHG Savings (kg)"]
    ev_trace5['x'] += tuple(x)
    ev_trace5['y'] += tuple(y)
    ev_trace5['name'] = "RACKSPACE 1 (GHG)"


############################################################################
# EV Location Data
############################################################################

# Station Locations
stat_names = []
stations = cps_ev_locs["StationName"].tolist()
for station in stations:
    if station not in stat_names:
        stat_names.append("Station {}:".format(station))
        stat_lat = cps_ev_locs["Latitude"].tolist()
        stat_long = cps_ev_locs["Longitude"].tolist()


############################################################################
# Bus Location Data
############################################################################

bus_ids = []
buses = bus_locs["BusID"].tolist()
for bus in buses:
    bus_ids.append("Bus ID: {}".format(bus))

bus_lat = bus_locs["Latitude"].tolist()
bus_long = bus_locs["Longitude"].tolist()


############################################################################
# VIA Ridership and PassCount
############################################################################
trace0 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="maroon"
    ),
    marker={
        "size": 15,
        "line": {
            "width": 0.5,
            "color": "white"
        }
    }
)

trace1 = go.Scatter(
    x=[],
    y=[],
    mode="lines",
    name="",
    opacity=0.7,
    line=dict(
        color="dodgerblue"
    ),
    marker={
        "size": 15,
        "line": {
            "width": 0.5,
            "color": "white"
        }
    }
)

trace2 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="green"
    ),
    marker={
        "size": 15,
        "line": {
            "width": 0.5,
            "color": "white"
        }
    }
)

trace3 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="maroon",
        dash='dash'
    )
)

trace4 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="dodgerblue",
        dash='dash'
    )
)

trace5 = go.Scatter(
    x=[],
    y=[],
    name="",
    mode="lines",
    opacity=0.7,
    line=dict(
        color="green",
        dash='dash'
    )
)


for i in via_riders.Route.unique():
    if i == 14 or i == 36:
        continue
    x = via_riders[via_riders["Route"] == i]["ServiceDate"]
    y = via_riders[via_riders["Route"] == i]["Ridership"]
    trace0['x'] += tuple(x)
    trace0['y'] += tuple(y)
    trace0['name'] = "2-BLANCO RD (Ridership)"

for i in via_riders.Route.unique():
    if i == 2 or i == 36:
        continue
    x = via_riders[via_riders["Route"] == i]["ServiceDate"]
    y = via_riders[via_riders["Route"] == i]["Ridership"]
    trace1['x'] += tuple(x)
    trace1['y'] += tuple(y)
    trace1['name'] = "14 Perrin Beitel (Ridership)"

for i in via_riders.Route.unique():
    if i == 2 or i == 14:
        continue
    x = via_riders[via_riders["Route"] == i]["ServiceDate"]
    y = via_riders[via_riders["Route"] == i]["Ridership"]
    trace2['x'] += tuple(x)
    trace2['y'] += tuple(y)
    trace2['name'] = "36-S. PRESA ST (Ridership)"

for i in via_riders.Route.unique():
    if i == 14 or i == 36:
        continue
    x = via_riders[via_riders["Route"] == i]["ServiceDate"]
    y = via_riders[via_riders["Route"] == i]["PassCount"]
    trace3['x'] += tuple(x)
    trace3['y'] += tuple(y)
    trace3['name'] = "2-BLANCO RD (PassCount)"

for i in via_riders.Route.unique():
    if i == 2 or i == 36:
        continue
    x = via_riders[via_riders["Route"] == i]["ServiceDate"]
    y = via_riders[via_riders["Route"] == i]["PassCount"]
    trace4['x'] += tuple(x)
    trace4['y'] += tuple(y)
    trace4['name'] = "14 Perrin Beitel (PassCount)"

for i in via_riders.Route.unique():
    if i == 2 or i == 14:
        continue
    x = via_riders[via_riders["Route"] == i]["ServiceDate"]
    y = via_riders[via_riders["Route"] == i]["PassCount"]
    trace5['x'] += tuple(x)
    trace5['y'] += tuple(y)
    trace5['name'] = "36-S. PRESA ST (PassCount)"


################################################################################
# Layout Properties and Pages Defined
################################################################################
app.layout = html.Div(
[
    dcc.Location(id="url"),
    dbc.NavbarSimple(
        children=[
            dbc.NavLink("Home", href="/page-1", id="page-1-link"),
            dbc.NavLink("EAA Data", href="/page-2", id="page-2-link"),
            dbc.NavLink("VIA Bus", href="/page-3", id="page-3-link"),
            dbc.NavLink("School District and City Services", href="/page-4", id="page-4-link"),
            dbc.NavLink("CPS Energy Services", href="/page-5", id="page-5-link"),
        ],
        brand="[ Team Hooli ]",
        brand_href="#",
        sticky="top",
        color="primary",
        dark=True,
    ),
    dbc.Container(id="page-content", className="pt-4"),
]
)

# this callback uses the current pathname to set the active state of the
# corresponding nav link to true, allowing users to tell see page they are on
@app.callback(


[Output(f"page-{i}-link", "active") for i in range(1, 6)],
[Input("url", "pathname")],
)

def toggle_active_links(pathname):
    if pathname == "/":
        # Treat page 1 as the homepage / index
        return True, False, False
    return [pathname == f"/page-{i}" for i in range(1, 6)]

################################################################################
# Body and Page Logic
################################################################################
@app.callback(Output("page-content", "children"), [Input("url", "pathname")])
def render_page_content(pathname):
    if pathname in ["/", "/page-1"]:
        return dbc.Container([
            dbc.Row([
                dbc.Col([
                    html.H2("What do you want to accomplish?"),
                    html.P(
                        """\
        > Our team is looking to aggregate data from multiple departments in San Antonio. 
        Our goal is to make data more transparent between the different organizations to initiate 
        the process of asking the right kind of questions to our city's most challenging problems. 
        Having the data that overlays the different facets of the City of San Antonio, VIA, and CPS 
        Energy will help us to determine what prevalent issues exist in context to one another and to 
        the greater issues of San Antonio (such as public transportation). Our intention is to implement 
        a dashboard that allows data to be shared, that will illuminate where problems exist through the 
        means of data visualization. This dashboard will centralize the data into one interface and create 
        an opportunity to see the relationships between VIA Bus routes, pollutants in the air, traffic, 
        and weather conditions. The key to finding an appropriate solution to a given problem, is to identify 
        where the problems exist through the help of data visualization that will address multiple concerns 
        in one simple, easy-to-use place."""
                    ),
                    html.Hr(),
                    html.H6("TEAM MEMBERS: David, Harry, Artem, Navin, Josh, Izzat"),
                    html.Hr(),
                    html.Iframe(style={"background-image": "url(assets/main_san-antonio.jpg)"}, height='420',
                                width="100%"),
                ]),
            ]),
            dbc.Row([
                dbc.Col([

                ]),
            ]),
            dbc.Row([
                dbc.Col([

                ]),
                dbc.Col([

                ]),
            ])
        ])
    elif pathname == "/page-2":
        return dbc.Container([
            dbc.Row([
                dbc.Col([
                    html.H3("Edward's Aquifer Data Correlation Matrix", style={"textAlign": "center"}),
                    html.Iframe(style={"background-image": "url(assets/EAA_Matrix_Cropped.png)",
                                       "display": "block", "margin": "auto"}, height='837',
                                width="720"),
                ])
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H3("Edward's Aquifer O2 Levels", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="Edward's Aquifer O2 Levels",
                        figure={
                            "data": [
                                go.Scatter(
                                    x=eaa_o2[eaa_o2["Name"] == i]["Date-Time"],
                                    y=eaa_o2[eaa_o2["Name"] == i]["O2 (Dis) mg/l"],
                                    mode="markers",
                                    # fill="tonexty",
                                    opacity=0.7,
                                    marker={
                                        "size": 15,
                                        "line": {
                                            "width": 0.5,
                                            "color": "white"
                                        }
                                    },
                                    name=i
                                ) for i in eaa_o2.Name.unique()
                            ],
                            "layout": go.Layout(
                                title="Edward's Aquifer O2 Levels",
                                xaxis=dict(
                                    title="Year 2018",
                                ),
                                yaxis=dict(title="O2 Levels (mg/l)"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    ),
                                )
                            )
                        }
                    )
                ]),
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H3("Edward's Aquifer pH Levels", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="Edward's Aquifer pH Levels",
                        figure={
                            "data": [
                                go.Scatter(
                                    x=eaa_ph[eaa_ph["Name"] == i]["Date-Time"],
                                    y=eaa_ph[eaa_ph["Name"] == i]["pH"],
                                    mode="markers",
                                    # fill="tonexty",
                                    opacity=0.7,
                                    marker={
                                        "size": 15,
                                        "line": {
                                            "width": 0.5,
                                            "color": "white"
                                        }
                                    },
                                    name=i
                                ) for i in eaa_ph.Name.unique()
                            ],
                            "layout": go.Layout(
                                title="Edward's Aquifer pH Levels",
                                xaxis=dict(title="Year 2018"),
                                yaxis=dict(title="pH Levels"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    ),
                                ),
                            )
                        }
                    )
                ]),
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H3("Edward's Aquifer Nephelom", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="Edward's Aquifer Nephelom",
                        figure={
                            "data": [
                                go.Scatter(
                                    x=eaa_neph[eaa_neph["Name"] == i]["Date-Time"],
                                    y=eaa_neph[eaa_neph["Name"] == i]["Nephelom NTU"],
                                    mode="markers",
                                    # fill="tonexty",
                                    opacity=0.7,
                                    marker={
                                        "size": 15,
                                        "line": {
                                            "width": 0.5,
                                            "color": "white"
                                        }
                                    },
                                    name=i
                                ) for i in eaa_o2.Name.unique()
                            ],
                            "layout": go.Layout(
                                title="Edward's Aquifer Nephelom",
                                xaxis=dict(
                                    title="Year 2018",
                                ),
                                yaxis=dict(title="Nephelom (NTU)"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    ),
                                )
                            )
                        }
                    )
                ]),
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H3("Edward's Aquifer Temperature in Celsius", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="Edward's Aquifer Temps",
                        figure={
                            "data": [
                                go.Scatter(
                                    x=eaa_temps[eaa_temps["Name"] == i]["Date-Time"],
                                    y=eaa_temps[eaa_temps["Name"] == i]["Temperature iC"],
                                    mode="markers",
                                    opacity=0.7,
                                    marker={
                                        "size": 15,
                                        "line": {
                                            "width": 0.5,
                                            "color": "white"
                                        }
                                    },
                                    name=i
                                ) for i in eaa_ph.Name.unique()
                            ],
                            "layout": go.Layout(
                                title="Edward's Aquifer Temperature (Celsius)",
                                xaxis=dict(title="Year 2018"),
                                yaxis=dict(title="Temps (Celsius)"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    ),
                                ),
                            )
                        }
                    )
                ]),
            ])
        ])
    elif pathname == "/page-3":
        return dbc.Container([
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H2("Bus Locations", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="Bus Locations",
                        figure={
                            "data": [
                                go.Scattermapbox(
                                    lat=bus_lat,
                                    lon=bus_long,
                                    mode="markers",
                                    opacity=0.7,
                                    marker=go.scattermapbox.Marker(
                                        size=9
                                    ),
                                    text=bus_ids
                                )
                            ],
                            "layout": go.Layout(
                                title="Bus Locations",
                                autosize=False,
                                width=1080,
                                height=620,
                                margin={'l': 0, 'r': 0, 't': 0, 'b': 0},

                                mapbox=go.layout.Mapbox(
                                    accesstoken=mapbox_access_token,
                                    bearing=0,
                                    center=go.layout.mapbox.Center(
                                        lat=29.42,
                                        lon=-98.49
                                    ),
                                    pitch=0,
                                    zoom=10
                                )
                            )
                        }
                    )
                ])
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H2("VIA Annual Bus Fare Activity Data", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="VIA Annual Bus Fare Data",
                        figure={
                            "data": [trace0, trace1, trace2, trace3, trace4, trace5],
                            "layout": go.Layout(
                                title="VIA Annual Ridership and PassCount (Top 3 Routes Only)",
                                xaxis=dict(title="Year 2018"),
                                yaxis=dict(title="Ridership"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    ),
                                ),
                            )
                        }
                    )
                ])
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H2("VIA Aug 28th Ridership w/ Pass and Bill Counts", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="VIA Aug 28th",
                        figure={
                            "data": [via_trace0, via_trace1, via_trace2],
                            "layout": go.Layout(
                                title="VIA Adherence Ridership, Pass Count, and Bill Count on Aug 28th (6 Buses Only)",
                                xaxis=dict(title="Year 2018"),
                                yaxis=dict(title="Ridership, Pass, Bill Counts"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=12
                                    ),
                                ),
                            )
                        }
                    )
                ])
            ]),
        ]),
    elif pathname == "/page-4":
        return dbc.Container([
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H2("School District and City Services", style={"textAlign": "center"}),
                    html.Hr(),
                    html.H4("Current Open 311 Service Calls in SA Council Districts", style={"textAlign": "center"}),

                    html.Iframe(src=f'https://jcdavenport.github.io/311webmap/', height='620', width='1080'),
                    html.Hr()

                ])
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H4("Police and Fire Service Stations within SA School Districts",
                            style={"textAlign": "center"}),

                    html.Iframe(src=f'https://jcdavenport.github.io/', height='620', width='1080'),
                    html.Hr()
                ])
            ])
        ]),
    elif pathname == "/page-5":
        return dbc.Container([
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H3("CPS Energy Services", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="EV Polution Savings",
                        figure={
                            "data": [
                                go.Scatter(
                                    x=cps_ev[cps_ev["StationName"] == i]["Start Date"],
                                    y=cps_ev[cps_ev["StationName"] == i]["GHG Savings (kg)"],
                                    mode="markers",
                                    # fill="tonexty",
                                    opacity=0.7,
                                    marker={
                                        "size": 15,
                                        "line": {
                                            "width": 0.5,
                                            "color": "white"
                                        }
                                    },
                                    name=i
                                ) for i in cps_ev.StationName.unique()
                            ],
                            "layout": go.Layout(
                                title="EV Pollution Savings",
                                xaxis=dict(
                                    title="Year 2018",
                                ),
                                yaxis=dict(title="EV Polution Savings"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    )
                                )
                            )
                        }
                    )
                ])
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H3("CPS Energy vs GHG Savings", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="EV Energy Efficiency",
                        figure={
                            "data": [ev_trace0, ev_trace1, ev_trace2, ev_trace3, ev_trace4, ev_trace5],
                            "layout": go.Layout(
                                title="CPS Energy vs GHG Savings (3 Sites Only)",
                                xaxis=dict(
                                    title="Year 2018",
                                ),
                                yaxis=dict(title="Energy vs GHG Savings"),
                                legend=dict(
                                    x=0.95,
                                    y=1,
                                    font=dict(
                                        size=10
                                    )
                                )
                            )
                        }
                    )
                ])
            ]),
            dbc.Row([
                dbc.Col([
                    html.Hr(),
                    html.H2("Station Coordinates", style={"textAlign": "center"}),
                    dcc.Graph(
                        id="Station Locations",
                        figure={
                            "data": [
                                go.Scattermapbox(
                                    lat=stat_lat,
                                    lon=stat_long,
                                    mode="markers",
                                    opacity=0.7,
                                    marker=go.scattermapbox.Marker(
                                        size=9,
                                        color="orangered"
                                    ),
                                    text=stat_names
                                )
                            ],
                            "layout": go.Layout(
                                title="Station Coordinates",
                                autosize=False,
                                width=1080,
                                height=620,
                                margin={'l': 0, 'r': 0, 't': 0, 'b': 0},

                                mapbox=go.layout.Mapbox(
                                    accesstoken=mapbox_access_token,
                                    style="dark",
                                    bearing=0,
                                    center=go.layout.mapbox.Center(
                                        lat=29.42,
                                        lon=-98.49
                                    ),
                                    pitch=0,
                                    zoom=10
                                )
                            )
                        }
                    )
                ])
            ]),
        ])
    # If the user tries to reach a different page, return a 404 message
    return dbc.Jumbotron(
        [
            html.H1("404: Not found", className="text-danger"),
            html.Hr(),
            html.P(f"The pathname {pathname} was not recognised..."),
        ]
    )


############################################################################
# Invoke Main
############################################################################
if __name__ == "__main__":
    app.run_server(port=5000)

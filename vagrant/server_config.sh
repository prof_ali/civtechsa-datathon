#!/usr/bin/env bash
sudo apt-get -y update
sudo apt-get -y install postgresql python-flask python-psycopg2
sudo apt-get -y install python-pip
sudo apt-get -y install python3-pip
pip3 install -r ./requirements.txt

su postgres -c 'createuser -drs vagrant'
su vagrant -c 'createdb san_antonio'
su vagrant -c 'psql san_antonio -f ./scripts/sql/schema_setup.sql'
su vagrant -c 'psql san_antonio -f ./scripts/sql/data_load.sql'

vagrantTip="[35m[1mThe shared directory is located at /vagrant\nTo access your shared files: cd /vagrant(B[m"
echo -e $vagrantTip > /etc/motd


# CivTechSA-Datathon

## Purpose

Our purpose is to create a Smart City application for the City of San Antonio. The only way to accomplish this, is by collecting data and finding relationships between different factors that affect this city. Making data more transparent between different departments of San Antonio could initiate great change, by understanding some of the most challenging problems we face. With the help of our dashboard, we look to visualize data into useful information for not just the city, but its many constituents.

This is currently an open source project by TAMUSA (Texas A&M University-San Antonio), started by students who are looking to help change the way we see our city's issues to make for a more "smart city".

## Getting started

To run our application, all you need to do is run the app.py program. In a localized environment, you will need to have a virtual environment that has all the binaries/libraries needed to run the app. Our recommendation is started with a VM, you can use vagrant for this purpose, otherwise run it as you see fit. You can also use our ansible script to install the components you need, handy script for starting with a server.

**NOTE**: If you are running this program in a directory owned by someone other than yourself, i.e `root`, you will need to use the `sudo` keyword before any of the following commands.

First, make sure you have python3 installed:
```
sudo apt update && sudo apt upgrade -y
sudo apt install -y python3 python3-venv python3-pip
```

Next, close our repo and then from the root of the project, create a python virtual environment
```
git clone https://gitlab.com/hooli-datathon/civtechsa-datathon.git
python3 -m venv venv
```

Then, install the packages needed and run the program:
```
pip3 install --upgrade pip
pip3 install -r requirements.txt
python3 app.py
```

## Caveats

When running the app, you will notice how slow the app is, this is because we are reading data from CSVs using the pandas library, this process is painfully slow. An idea way of handling this is putting the CSV data into a database. Reading from a database is infinitely faster and far more dynamic for the purposes of this app.

In the scripts directory, there are attempts to do this. The Data Sources is the majority of the weight in this code base, which should be eliminated.
